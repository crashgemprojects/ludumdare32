﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

public class CreateSkill{

	[MenuItem("Assets/Create/Skill")]
	public static void CreateSkills()
	{
		Skill skill = ScriptableObject.CreateInstance<Skill> ();
		
		string path = "Assets";
		foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets)) {
			path = AssetDatabase.GetAssetPath (obj);
			if (File.Exists (path)) {
				path = Path.GetDirectoryName (path);
			}
			break;
		}
		
		AssetDatabase.CreateAsset (skill, path + "/New Skill.asset");
		EditorUtility.FocusProjectWindow ();
		Selection.activeObject = skill;
	}
}
