﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Skill))]
public class EditorSkill : Editor
{

	public override void OnInspectorGUI ()
	{
		Skill skill = (Skill)target;
	
		skill.Name = EditorGUILayout.TextField ("Skill Name: ", skill.Name);
		skill.Icon = (Sprite)EditorGUILayout.ObjectField ("Icon: ", skill.Icon, typeof(Sprite), false);
		skill.Cost = EditorGUILayout.IntField ("Skill Cost: ", skill.Cost);
		skill.Clip = (AudioClip)EditorGUILayout.ObjectField ("Sound: ", skill.Clip, typeof(AudioClip), false);
		skill.Cooldown = EditorGUILayout.FloatField ("Cooldown Time: ", skill.Cooldown);

		if (GUILayout.Button ("Add Trait")) {

			skill.Traits.Add (new Trait ());
		}

		for (int i = 0; i< skill.Traits.Count; i++) {
			Trait trait = skill.Traits [i];
		
			if (GUILayout.Button ("Remove")) {
				skill.Traits.RemoveAt (i);
				i--;
			}
			trait.Type = (TraitType)EditorGUILayout.EnumPopup ("Type: ", trait.Type);

			switch (trait.Type) {
				
			case TraitType.Melee:
				trait.ParamOne = EditorGUILayout.IntField ("Damage: ", trait.ParamOne);
				trait.ParamTwo = EditorGUILayout.IntField ("Reach: ", trait.ParamTwo);
				
				break; 
			case TraitType.Range:
				
				trait.Projectile = (Projectile)EditorGUILayout.ObjectField ("Projectile: ", trait.Projectile, typeof(Projectile), false);
				
				break;
			case TraitType.Summon:
				
				AgentTemplate npc = (AgentTemplate)EditorGUILayout.ObjectField ("Creature", trait.Template, typeof(AgentTemplate), false);
				GUILayout.Label ("AgentTemplate type must be NPC");
				
				if (npc != null) {
					if (npc.Type == AgentType.NPC) {
						trait.Template = npc;
					}
				}
				break;
			}
		}

		EditorUtility.SetDirty (skill);
	}

}
