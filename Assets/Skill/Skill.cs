﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SkillSlot
{
	Primary, 
	Secondary
}

public class Skill : ScriptableObject, IEnumerable
{
	public string Name;
	public Sprite Icon;
	public int Cost;
	public float Cooldown;
	public AudioClip Clip;
	public List<Trait> Traits = new List<Trait>();
	private bool _onCooldown;

	public bool OnCooldown {
		get {
			return this._onCooldown;
		}
	}

	public void Use (Creature creature, SkillSlot slot)
	{
		creature.PlayAudio (this.Clip);
		Ray2D ray = new Ray2D (creature.Transform.position, creature.CachedAxis);
		foreach (Trait trait in this.Traits) {
			switch (trait.Type) {
			case TraitType.Melee:

				//See if we can hit objects. 
				RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, (float)trait.ParamTwo);

				if (hit != null) {
					if (hit.collider.gameObject.GetComponent<MonoAgent> () != null) {
						Agent agent = hit.collider.gameObject.GetComponent<MonoAgent> ().Agent;

						if (agent.Type == AgentType.NPC || agent.Type == AgentType.PC) {

							Creature c = (Creature)agent;
							if (creature.Side != c.Side) {
								c.TakeDamage (trait.ParamOne);
							}
						}
					}
				}

				break;

			case TraitType.Range:

				if (trait.Projectile != null) {

					Projectile p = (Projectile)GameObject.Instantiate (trait.Projectile, ray.GetPoint (1.5f), Quaternion.identity);
					Physics2D.IgnoreCollision(p.GetComponent<Collider2D>(), creature.gameObject.GetComponent<Collider2D>());

					p.Fire (ray.direction, creature);

					if (slot == SkillSlot.Primary) {
						creature.SetPrimaryProjectile (p);
					} else {
						creature.SetSecondaryProjectile (p);
					}
				}
				break;

			case TraitType.Summon:

				if (trait.Template != null) {
					NPC c = new NPC (trait.Template, ray.GetPoint (1), Quaternion.identity);
					c.SetDefaultTarget(creature);
				}

				break;

			}
		}
	}

	public IEnumerator RunCooldown ()
	{
		this._onCooldown = true;
		yield return new WaitForSeconds (this.Cooldown);
		this._onCooldown = false;
	}

	#region IEnumerable implementation

	public IEnumerator GetEnumerator ()
	{
		foreach (Trait trait in this.Traits) {
			yield return trait;
		}
	}

	#endregion
}
