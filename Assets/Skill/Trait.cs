﻿using UnityEngine;
using System.Collections;

public enum TraitType{
	Melee, 
	Range, 
	Summon
}
	
[System.Serializable]
public class Trait{
	public TraitType Type;
	public int ParamOne; 
	public int ParamTwo;
	public float Cooldown;
	public Projectile Projectile;
	public AgentTemplate Template;
}