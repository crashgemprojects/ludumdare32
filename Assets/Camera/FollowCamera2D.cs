﻿using UnityEngine;
using System.Collections;

public class FollowCamera2D : MonoBehaviour {
	public Agent Target;
	public float Height;

	void Update()
	{
		if (this.Target != null) {
			this.transform.position = new Vector3(this.Target.gameObject.transform.position.x, this.Target.gameObject.transform.position.y, this.Target.gameObject.transform.position.z + this.Height);
			this.transform.LookAt(this.Target.gameObject.transform.position);
		}
	}
}
