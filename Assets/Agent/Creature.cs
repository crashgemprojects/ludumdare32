﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum CreatureMoveType
{
	Rigidbody, 
	Navmesh
}

public delegate void animHandler ();

public class Creature : Agent
{
	protected Vector2 axis;
	private Vector2 _cachedAxis;
	protected Skill primarySkill;
	protected Skill secondarySkill;
	protected Projectile primarySkillProjectile;
	protected Projectile secondarySkillProjectile;
	protected iTargetable defaultTarget;
	protected animHandler animSyntax;
	protected bool canMove = true;
	private float _defaultSpeed;
	private CreatureMoveType _moveType;



	public Creature (AgentTemplate template, Vector3 position, Quaternion rotation):base(template, position, rotation)
	{
		this._defaultSpeed = template.DefaultSpeed;
		this._moveType = template.CreatureMoveType;

		if (template.DefaultPrimarySkill != null) {
			this.primarySkill = (Skill)ScriptableObject.Instantiate (template.DefaultPrimarySkill);
		}

		if (template.DefaultSecondarySkill != null) {
			this.secondarySkill = (Skill)ScriptableObject.Instantiate (template.DefaultSecondarySkill);
		}

		if (this.Type == AgentType.NPC) {


		} else if (this.Type == AgentType.PC) {
		
		}

		//Playing Animation
		this.mBase.UpdateEvent += () => {
			if(this.animSyntax != null)
			{
				this.animSyntax();
			}else{
				this.PlayAnim(this.state);
			}
		};

		//Caching direction so we have a default, none zeroed direction to fall back on. 
		this.mBase.UpdateEvent += () => {

			if(this.axis != Vector2.zero)
			{
				this._cachedAxis = this.axis;
			}else if(this._cachedAxis == Vector2.zero)
			{
				this._cachedAxis = new Vector2(-1,0);
			}
		};

		this.mBase.UpdateEvent += this._move;
	}

	public Vector3 CachedAxis{
		get{
			return this._cachedAxis;
		}
	}

	public void SetPrimaryProjectile (Projectile p)
	{
		this.primarySkillProjectile = p;
	}

	public void SetSecondaryProjectile (Projectile p)
	{
		this.secondarySkillProjectile = p;
	}

	public void SetDefaultTarget (iTargetable target)
	{
		this.defaultTarget = target;
	}

	public Vector2 Axis {
		get {
			return this.axis;
		}
	}

	protected void useSkill (Skill skill, SkillSlot slot)
	{
		if (!skill.OnCooldown) {
			skill.Use (this, slot);
			this.state = AgentState.Action;
			this.mBase.StartCoroutine (skill.RunCooldown ());
			this.mBase.StartCoroutine (this._stateDelay (skill.Cooldown, AgentState.Idle));
		}

	}

	private void _move ()
	{
		Rigidbody2D r = this.gameObject.GetComponent<Rigidbody2D> ();

		if (this.axis != Vector2.zero && this.canMove) {
			Vector2 force = new Vector3 (this.axis.x * this._defaultSpeed, this.axis.y * this._defaultSpeed) * 5;
			this.state = AgentState.Move;

			if (this.axis != Vector2.zero) {
			
				r.AddForce (force, ForceMode2D.Force);
				r.velocity = new Vector3 (Mathf.Clamp (r.velocity.x, -1 * this._defaultSpeed, this._defaultSpeed), Mathf.Clamp (r.velocity.y, -1 * this._defaultSpeed, this._defaultSpeed));

				if (this._cachedAxis.x > 0) {
					this.gameObject.transform.localScale = new Vector3 (1, 1, 1);
				} else {
					this.gameObject.transform.localScale = new Vector3 (-1, 1, 1);
				}

				if (this.animSyntax != null) {
					this.animSyntax ();
				} else {
					this.PlayAnim (this.State);
				}
			} 
		} else {
			r.angularVelocity = 0; 
			r.velocity = Vector2.zero;

			if(this.state != AgentState.Die && this.canMove)
			{
				this.state = AgentState.Idle;
			}
		}
	}



	private IEnumerator _stateDelay (float duration, AgentState targetState)
	{
		this.canMove = false;
		yield return new WaitForSeconds (duration);
		this.state = targetState;
		this.canMove = true;
	}
}

public struct TargetWeight
{
	private float _weight;
	private float _endTime;

	public TargetWeight (float weightMod, float endTime)
	{
		this._weight = weightMod;
		this._endTime = endTime;
	}

	public float Weight {
		get {
			return this._weight;
		}
	}

	public float EndTime {
		get {
			return this._endTime;
		}
	}
}