﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GIA : Agent, iTargetable {

	protected bool grown = true;
	private delegate IEnumerator giaCallback();
	private float _maturity = 10;
	private float _currentGrowth = 0;
	private float _growthRate = 1;
	private event giaCallback _startEvent;
	private List<Coroutine> _behaviorsRunning = new List<Coroutine> ();
	private List<giaCallback> _behaviors = new List<giaCallback> ();


	public GIA(AgentTemplate template, Vector3 position, Quaternion rotation):base(template, position, rotation)
	{

		this.gameObject.GetComponent<Rigidbody2D> ().isKinematic = true;


		foreach (GiaVerbs verb in template.Verbs) {
			switch(verb)
			{
				case GiaVerbs.Grow:
				this._behaviors.Add(this._grow);
				break;

				case GiaVerbs.Heal:
				break;
			}
		}
	}

	public void Start()
	{
		if (this._behaviorsRunning.Count > 0) {
			this._behaviorsRunning.ForEach(x => this.mBase.StopCoroutine(x));
			this._behaviorsRunning = new List<Coroutine>();
		}

		if (this._behaviors.Count > 0) {

			foreach(giaCallback behavior in this._behaviors)
			{
				this._behaviorsRunning.Add(this.mBase.StartCoroutine(behavior()));
			}
		}
	}

	protected float growthProgress{
	
		get{
			return this._currentGrowth/this._maturity;
		}
	
	}

	private IEnumerator _grow()
	{
		this.grown = false;
		while(this._currentGrowth <= this._maturity)
		{
			this._currentGrowth+= this._growthRate;
			yield return new WaitForSeconds(1.5f);
		}

		this.grown = true;
	}


}
