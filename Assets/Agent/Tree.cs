﻿using UnityEngine;
using System.Collections;

public class Tree : GIA {

	private PC _exiting;

	public Tree(AgentTemplate template, Vector3 position, Quaternion rotation):base(template, position, rotation)
	{
		CircleCollider2D r = this.gameObject.AddComponent<CircleCollider2D> ();
		r.radius = 2;
		r.isTrigger = true;

		this.mBase.Trigger2DEnterEvent += (c) => {
			
			if(c.GetComponent<MonoAgent>())
			{
				Agent a = c.GetComponent<MonoAgent>().Agent;
				if(a.Type == AgentType.PC && this.grown)
				{
					PC pc = (PC)a;
					pc.SetNearTree(this);
				}
			}
		};
		
		this.mBase.Trigger2DExitEvent += (c) => {
			if(c.GetComponent<MonoAgent>())
			{
				Agent a = c.GetComponent<MonoAgent>().Agent;
				if(a.Type == AgentType.PC && this.grown)
				{
					PC pc = (PC)a;
					pc.UnsetNearTree(this);
				}
			}
		};

		this.mBase.UpdateEvent += () => {

			if(this.growthProgress <= 0.25f)
			{
				this.PlayAnim("Seed");
			}else if(this.growthProgress <= 0.5f)
			{
				this.PlayAnim("MidGrow");
			}else if(this.growthProgress < 0.75f)
			{
				this.PlayAnim("LateGrow");
			}else{
				this.PlayAnim("FullGrow");
			}

		};

		this.mBase.UpdateEvent += () => {
			if(this._exiting != null)
			{
				if(Vector2.Distance(this._exiting.Transform.position, this.Transform.position) > 0.5)
				{
					Physics2D.IgnoreCollision(this._exiting.gameObject.GetComponent<Collider2D>(), this.gameObject.GetComponent<BoxCollider2D>(), false);
				}
			}
		};

	}

	public void PCExit(PC pc)
	{
		this._exiting = pc;
		Physics2D.IgnoreCollision(this._exiting.gameObject.GetComponent<Collider2D>(), this.gameObject.GetComponent<BoxCollider2D>(), true);
	}

}
