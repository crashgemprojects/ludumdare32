﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(AgentTemplate))]
public class EditorAgentTemplate : Editor {
	private GiaVerbs _verb;

	public override void OnInspectorGUI ()
	{
		AgentTemplate template = (AgentTemplate)target;


		template.Animator = (RuntimeAnimatorController)EditorGUILayout.ObjectField ("Animator: ", template.Animator, typeof(RuntimeAnimatorController), false);
		template.Avatar = (GameObject)EditorGUILayout.ObjectField ("Avatar: ", template.Avatar, typeof(GameObject), false);
		template.Mode = AgentMode.TwoDim;//(AgentMode)EditorGUILayout.EnumPopup ("Agent Mode: ", template.Mode);
		template.Type = (AgentType)EditorGUILayout.EnumPopup ("Agent Type: ", template.Type);
		template.Side = (AgentSide)EditorGUILayout.EnumPopup ("Agent Side: ", template.Side);
		template.Health = EditorGUILayout.IntField ("Health: ", template.Health);

		template.Toughness = EditorGUILayout.IntField ("Toughness: ", template.Toughness);
		template.DefaultTargetWeight = EditorGUILayout.FloatField ("Target Weight: ", template.DefaultTargetWeight);

		if (template.Type == AgentType.NPC || template.Type == AgentType.PC) {

			template.CreatureMoveType = CreatureMoveType.Rigidbody;
			template.DefaultSpeed = EditorGUILayout.FloatField ("Speed: ", template.DefaultSpeed);
			template.DefaultPrimarySkill = (Skill)EditorGUILayout.ObjectField ("Primary Skill: ", template.DefaultPrimarySkill, typeof(Skill), false);
			template.DefaultSecondarySkill = (Skill)EditorGUILayout.ObjectField ("Secondary Skill: ", template.DefaultSecondarySkill, typeof(Skill), false);

			if (template.Type == AgentType.NPC) {
				template.MovementType = (NPCMovementType)EditorGUILayout.EnumPopup ("Movement Type: ", template.MovementType);
			}
		} else if (template.Type == AgentType.Gia) {
		
			this._verb = (GiaVerbs)EditorGUILayout.EnumPopup("Verbs: ", this._verb);
			if(GUILayout.Button("Add"))
			{
				if(!template.Verbs.Contains(this._verb))
				{
					template.Verbs.Add(this._verb);
				}
			}

			for(int i = 0; i< template.Verbs.Count; i++)
			{
				GUILayout.BeginHorizontal();

				GUILayout.Label(template.Verbs[i].ToString());
				if(GUILayout.Button("X"))
				{
					template.Verbs.RemoveAt(i);
					i--;
				}

				GUILayout.EndHorizontal();
			}
		}

		EditorUtility.SetDirty (template);
	}
}
