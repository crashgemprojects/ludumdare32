﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

public class CreateAgentTemplate{

	[MenuItem("Assets/Create/AgentTemplate")]
	public static void CreateCreature()
	{
		AgentTemplate template = ScriptableObject.CreateInstance<AgentTemplate>();
		
		string path = "Assets";
		foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets))
		{
			path = AssetDatabase.GetAssetPath(obj);
			if (File.Exists(path))
			{
				path = Path.GetDirectoryName(path);
			}
			break;
		}
		
		AssetDatabase.CreateAsset (template, path+"/New Agent Template.asset");
		EditorUtility.FocusProjectWindow ();
		Selection.activeObject = template;
	}
}
