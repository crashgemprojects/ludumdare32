﻿using UnityEngine;
using System.Collections;

public enum PCState{
	Squirl,
	Mech

}

public class PC : Creature {

	private PCState _pcState;
	private int _nutCount;
	private bool _isPlanting;
	private bool _enterExit;
	private TraitType _actionType;
	private GIA _mechInRange;

	public PC(AgentTemplate template, Vector3 position, Quaternion rotation):base(template, position, rotation)
	{
		//Testing config
		this.mBase.StartEvent += () => {
			this._nutCount = 5;

		};

		this.animSyntax = () => {

			string name = null;
			if(this.state == AgentState.Action && this._pcState == PCState.Mech)
			{
				name = this._pcState.ToString()+this._actionType;
			}else{
				name = this._pcState.ToString()+this.state.ToString();
			}

			this.PlayAnim(name);
		};

		this.mBase.UpdateEvent += this._getInput;
		this.mBase.UpdateEvent += this._action;

	}

	public void SetNearTree(GIA gia)
	{
		this._mechInRange = gia;
	}
	
	public void UnsetNearTree(GIA gia)
	{
		this._mechInRange = gia;
	}

	public int NutCount{
		get{
			return this._nutCount;
		}
	}

	public void GetNut()
	{
		this._nutCount++;
	}

	private void _getInput()
	{
		if (this.state != AgentState.Action && this.state != AgentState.Die) {
			float x = 0;
			if (Input.GetKey (KeyCode.A)) {
				x = -1;
			} else if (Input.GetKey (KeyCode.D)) {
				x = 1;
			} 
		
			float y = 0; 
			if (Input.GetKey (KeyCode.W)) {
				y = 1;
			} else if (Input.GetKey (KeyCode.S)) {
				y = -1;
			}
	
			if (Input.GetKey (KeyCode.Q) && !this._enterExit) {
				Debug.LogWarning ("Entering or Exiting: " + (this._mechInRange != null));

				if (this._pcState == PCState.Mech) {

					if(Round.Self.MagicPuff != null)
					{
						ParticleSystem magic = (ParticleSystem)GameObject.Instantiate(Round.Self.MagicPuff, this.Transform.position, Quaternion.identity);
					}

					this._pcState = PCState.Squirl;
					GIA g = new GIA (Round.Self.TreeTemplate, this.Transform.position, Quaternion.identity);
					g.Health.Change (g.Health.Max - (g.Health.Max * this.Health.Ratio));


				} else if (this._pcState == PCState.Squirl && this._mechInRange != null) {

					if(Round.Self.MagicPuff != null)
					{
						ParticleSystem magic = (ParticleSystem)GameObject.Instantiate(Round.Self.MagicPuff, this.Transform.position, Quaternion.identity);
					}
					this._pcState = PCState.Mech;
					this._mechInRange.Clear ();
					this._mechInRange = null;
				}



				this.mBase.StartCoroutine (this._enterExitCooldown ());
			}

			this.axis = new Vector2 (x, y);
		} else {
			this.axis = Vector2.zero;
		}
	}

	private void _action ()
	{

		if (Input.GetMouseButton (0) && !this.primarySkill.OnCooldown) {
		
			if(this._pcState == PCState.Mech)
			{
				this._actionType = this.primarySkill.Traits[0].Type;
				this.useSkill(this.primarySkill, SkillSlot.Primary);
			}else{

				this._plantTree();
			}
		}
		
		if (Input.GetMouseButton (1) && !this.secondarySkill.OnCooldown) {
			if(this._pcState == PCState.Mech)
			{
				this._actionType = this.primarySkill.Traits[0].Type;
				this.useSkill(this.secondarySkill, SkillSlot.Secondary);
			}
		}

	}

	private void _plantTree()
	{
		if (this._nutCount > 0 && Round.Self.TreeTemplate != null && !this._isPlanting) {
			Ray2D r = new Ray2D(this.Transform.position, this.CachedAxis);
			RaycastHit2D hit = Physics2D.Raycast(r.GetPoint(0.5f), this.CachedAxis, 1);

			if(hit.collider == null)
			{

				Tree g = new Tree(Round.Self.TreeTemplate, r.GetPoint(1f), Quaternion.identity);
				g.Start();
				this.mBase.StartCoroutine(this._plantingCooldown());
				this._nutCount--;

			}
		}
	}

	private IEnumerator _plantingCooldown()
	{
		this._isPlanting = true;
		this.state = AgentState.Action;
		yield return new WaitForSeconds(1f);
		this.state = AgentState.Idle;
		this._isPlanting = false;
	}

	private IEnumerator _enterExitCooldown()
	{
		this._enterExit = true;
		yield return new WaitForSeconds (1.5f);
		this._enterExit = false;
	}
}
