﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class NPC : Creature {
	private List<iTargetable> _enemies = new List<iTargetable> ();
	private iTargetable _target;

	public NPC(AgentTemplate template, Vector3 position, Quaternion rotation):base(template, position, rotation)
	{
		CircleCollider2D trigger = this.gameObject.AddComponent<CircleCollider2D> ();
		trigger.radius = 2;
		trigger.isTrigger = true;
		
		this._pickTarget();

		this.animSyntax = () => {

			this.PlayAnim(this.state);
		};

		this.mBase.Collision2DEnterEvent += (c) => {

			if(c.gameObject.GetComponent<MonoAgent>())
			{
				Agent a = c.gameObject.GetComponent<MonoAgent>().Agent;

				if(a.Side == this.Side && a.Type == AgentType.NPC)
				{
					Physics2D.IgnoreCollision(a.gameObject.GetComponent<BoxCollider2D>(), gameObject.GetComponent<BoxCollider2D>());
				}

			}
		};
		this.mBase.Trigger2DEnterEvent += this._detect;
		this.mBase.Trigger2DExitEvent += this._forget;
		this.mBase.UpdateEvent += this._aiUseSkill;


		Round.Self.AddAgent (this);

		this.Events [AgentState.Die].Add (() => {
			this.mBase.StartCoroutine(this._dieHandler());
		});
		
		switch (template.MovementType) {
		case NPCMovementType.Burrow:
			this.mBase.UpdateEvent += this._moveBurrow;
			break;
			
		case NPCMovementType.Simple:
			this.mBase.UpdateEvent += this._moveSimple;
			break;
			
		case NPCMovementType.Teleport:
			this.mBase.UpdateEvent += this._moveTeleport;
			break;
		}
	}

	public void SetTarget(iTargetable target)
	{
		if (target != null) {
			this.defaultTarget = target; 
			this._target = target;
		}
	}

	private IEnumerator _dieHandler()
	{
		this.state = AgentState.Die;
		Debug.Log ("Dieing");
		yield return new WaitForSeconds (2f);
		this.Clear ();
		Round.Self.RemoveAgent(this);
	}

	private void _aiUseSkill()
	{
		if (this.state != AgentState.Die) {
			this._checkUseSkill (this.primarySkill, SkillSlot.Primary);
			this._checkUseSkill (this.secondarySkill, SkillSlot.Secondary);
		}
	}
	
	private void _checkUseSkill(Skill skill, SkillSlot slot)
	{
		if (skill != null) {
			if (!skill.OnCooldown && this._target != null) {
				foreach (Trait trait in skill) {


					if (trait.Type == TraitType.Melee) {
						RaycastHit2D hit = Physics2D.Raycast (this.Transform.position, this.CachedAxis, trait.ParamTwo);
						if (hit.collider != null) {
							if (hit.collider.gameObject.GetComponent<MonoAgent> ()) {
								Agent a = hit.collider.gameObject.GetComponent<MonoAgent> ().Agent;
								if (a.Side != this.Side) {
									this.useSkill (skill, slot);
								}
							}
						}
						break;
					} else if (trait.Type == TraitType.Range) {
						RaycastHit2D hit = Physics2D.Raycast (this.Transform.position + this.CachedAxis, this.CachedAxis, trait.Projectile.Range);
						if (hit.collider != null) {
							if (hit.collider.gameObject.GetComponent<MonoAgent> ()) {

								Agent a = hit.collider.gameObject.GetComponent<MonoAgent> ().Agent;
								if (a.Side != this.Side) {
									Debug.LogWarning ("USING SKILL");
									this.useSkill (skill, slot);
								}
							}
						}
						break;
					} else if (trait.Type == TraitType.Summon) {
						this.useSkill (skill, slot);
						break;
					}
				}
			}
		}
	}

	private void _pickTarget ()
	{
		if (this._target != null) {
			
			iTargetable target = this._enemies.Find(x => x.TargetWeight > this._target.TargetWeight);
			
			if(target != null)
			{
				this._target = target;
				this._target.Events[AgentState.Die].Add(this._targetDie);
			}else{
				Debug.Log("Not new selection");
			}
		} else {
			
			if(this._enemies.Count > 0)
			{
				List<iTargetable> targets = this._enemies.OrderBy(x => x.TargetWeight).ToList();
				this._target = targets[0];
				this._target.Events[AgentState.Die].Add(this._targetDie);
			}else{
				if(this.defaultTarget != null)
				{
					this._target = this.defaultTarget;
				}
			}
		}
	}
	
	private void _detect (Collider2D target)
	{
		if (target.GetComponent<MonoAgent> ()) {
			Agent agent = target.GetComponent<MonoAgent> ().Agent;
			if (agent.Type == AgentType.NPC || agent.Type == AgentType.PC) {
				if (agent.Side != this.Side) {
					this._enemies.Add ((iTargetable)agent);
					this._pickTarget();
				}
			}
		}
	}
	
	private void _forget (Collider2D target)
	{
		if (target.GetComponent<MonoAgent> ()) {
			Agent agent = target.GetComponent<MonoAgent> ().Agent;
			if (agent.Type == AgentType.NPC || agent.Type == AgentType.PC) {
				if (agent.Side != this.Side) {
					int i = this._enemies.FindIndex (x => x.Transform.gameObject.GetInstanceID () == agent.gameObject.GetInstanceID ());
					if (i != -1) {
						
						this._enemies.RemoveAt (i);
						
						//This is awful code and should be rewritten.
						if(this._target != null)
						{
							this._target.Events[AgentState.Die].Remove(this._targetDie);
						}
						
						this._target = null;
						this._pickTarget();
					} else {
						Debug.Log ("Enemies not found");
					}
				}
			}
		}
	}
	
	private void _targetDie()
	{
		this._target = null;
		this._pickTarget ();
	}
	
	private void _moveSimple ()
	{
		if (this.state != AgentState.Die) {
			if (this._target != null) {
				if (Vector2.Distance (this._target.Transform.position, this.Transform.position) > 0.75) {
					this.axis = (this._target.Transform.position - this.Transform.position).normalized;
					return;
				}
			} 
		}
		this.axis = Vector2.zero;
	}
	
	private void _moveBurrow ()
	{
		
	}
	
	private void _moveTeleport ()
	{
		
	}

}
