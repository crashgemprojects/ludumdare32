﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum AgentSide{
	None,
	Friendly, 
	Enemy
}

public enum AgentMode{
	TwoDim, 
	ThreeDim
}

public enum AgentState{
	Idle, 
	Move, 
	Action, 
	Die
}

public class Agent: iTargetable{

	private Stat _health; 
	private Stat _toughness;
	private int _energy;

	private AgentSide _side;
	private AgentMode _mode;
	private AgentType _type;


	protected AgentState state;


	private AgentStateEvents _events = new AgentStateEvents ();

	public GameObject gameObject;
	protected MonoBase mBase;
	protected Animator anim;

	private float _targetWeight;
	private float _cachedWeightMod;



	private List<TargetWeight> _weightMod = new List<TargetWeight> ();

	public Agent(AgentTemplate template, Vector3 position, Quaternion rotation)
	{
		this.gameObject = GameObject.Instantiate (template.Avatar);
		this.gameObject.transform.position = position;
		this.gameObject.transform.rotation = rotation;


		//Interface for collision handling
		MonoAgent agent = this.gameObject.AddComponent<MonoAgent> ();
		agent.Agent = this;


		//Adding audio player
		AudioSource src = this.gameObject.AddComponent<AudioSource> ();


		//Gives access to a subset of the MonoBehavior messages as events. 
		this.mBase = this.gameObject.AddComponent<MonoBase> ();

		//Rigging up the animator.
		this.anim = this.gameObject.GetComponent<Animator> ();
		this.anim.runtimeAnimatorController = RuntimeAnimatorController.Instantiate(template.Animator);
		
		this._health = new Stat(template.Health);
		this._toughness = new Stat(template.Toughness);
		
		this._type = template.Type;
		this._mode = template.Mode;
		this._side = template.Side;

		this._targetWeight = template.DefaultTargetWeight;

		switch(this._mode)
		{
		case AgentMode.TwoDim:

			if(this.gameObject.GetComponent<SpriteRenderer>() == null)
			{
				this.gameObject.AddComponent<SpriteRenderer>();
			}

			if(this.gameObject.GetComponent<Rigidbody2D>() == null)
			{
				this.gameObject.AddComponent<Rigidbody2D>();
			}
				Rigidbody2D r = this.gameObject.GetComponent<Rigidbody2D>();
				r.fixedAngle = true;
				r.gravityScale = 0;


			if(this.gameObject.GetComponent<Collider2D>() == null)
			{
				this.gameObject.AddComponent<PolygonCollider2D>();
			}


			break;
		case AgentMode.ThreeDim:
			Debug.LogError("Three Dim Agent Not Implemented");
			break;
		}

		this.mBase.UpdateEvent += this._updateWeightMod;
		this.mBase.UpdateEvent += () => {
			this.gameObject.GetComponent<SpriteRenderer>().sortingLayerID = (int)this.gameObject.transform.position.y;
		};
	}

	public AgentState State{
		get{
			return this.state;
		}
	}

	public AgentType Type{
		get{
			return this._type;
		}
	}

	public AgentSide Side{
		get{
			return this._side;
		}
	}

	public Stat Health{
		get{
			return this._health;
		}
	}

	public Stat Toughness{
		get{
			return this._toughness;
		}
	}

	public AgentStateEvents Events{
		get{
			return this._events;
		}
	}

	public Transform Transform {
		get {
			return this.gameObject.transform;
		}
	}

	public void Clear()
	{
		GameObject.Destroy (this.gameObject);
	}

	public int TakeDamage (int damage)
	{
		//int result = Mathf.Clamp ((this.Toughness.Current - Mathf.Abs (damage)), 0, this.Health.Current);
		this.Health.Change (-1 * Mathf.Abs(damage));

		if (this.Health.Current <= 0) {
			this.Events[AgentState.Die].Run();
		}

		return damage;
	}

	public void PlayAudio(AudioClip clip)
	{
		if (clip != null) {
			AudioSource src = this.gameObject.GetComponent<AudioSource> ();
			src.PlayOneShot (clip);
		}
	}

	public void PlayAnim (string name)
	{
		int hash = Animator.StringToHash (name);
		if (this.anim.GetCurrentAnimatorStateInfo (0).fullPathHash != hash) {
			this.anim.Play (hash);
			this.gameObject.GetComponent<BoxCollider2D>().size = this.gameObject.GetComponent<SpriteRenderer>().bounds.size;
		}
	}
	
	public void PlayAnim (AgentState state)
	{
		this.PlayAnim (state.ToString ());
	}
	
	public float TargetWeight {
		get {
			return this._targetWeight;
		}
	}

	public void ModTargetWeight (float chng, float duration)
	{
		this._weightMod.Add (new TargetWeight (chng, Time.time + duration));
	}

	private void _updateWeightMod ()
	{
		if (this._weightMod.Count > 0) {
			this._cachedWeightMod = 0;
			
			for (int i = 0; i< this._weightMod.Count; i++) {
				if (this._weightMod [i].EndTime <= Time.time) {
					this._weightMod.RemoveAt (i);
					i--;
				} else {
					this._cachedWeightMod += this._weightMod [i].Weight;
				}
			}
		}
	}
}
