﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum AgentType{
	Structure,
	Gia,
	NPC,
	PC
}

public enum NPCMovementType{
	Simple, 
	Burrow, 
	Teleport
}

public enum GiaVerbs{
	Grow, 
	Heal
}

public class AgentTemplate : ScriptableObject {
	public GameObject Avatar;
	public RuntimeAnimatorController Animator;
	public AgentMode Mode;
	public AgentType Type;
	public AgentSide Side;
	public int Health;
	public int Toughness;
	public CreatureMoveType CreatureMoveType;
	public float DefaultSpeed;
	public Skill DefaultPrimarySkill;
	public Skill DefaultSecondarySkill;
	public NPCMovementType MovementType;
	public float DefaultTargetWeight;
	public List<GiaVerbs> Verbs = new List<GiaVerbs>();
}
