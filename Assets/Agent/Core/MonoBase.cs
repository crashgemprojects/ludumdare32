﻿using UnityEngine;
using System.Collections;

public delegate void MonoBaseEvent();
public delegate void MonoBaseCollision(Collision c);
public delegate void MonoBaseCollision2D(Collision2D c);
public delegate void MonoBaseTrigger(Collider c);
public delegate void MonoBaseTrigger2D(Collider2D c);

public class MonoBase : MonoBehaviour {
	public event MonoBaseEvent AwakeEvent;
	public event MonoBaseEvent StartEvent;
	public event MonoBaseEvent UpdateEvent;
	public event MonoBaseEvent FixedUpdateEvent;
	public event MonoBaseCollision CollisionEnterEvent;
	public event MonoBaseCollision CollisionStayEvent;
	public event MonoBaseCollision CollisionExitEvent;
	public event MonoBaseCollision2D Collision2DEnterEvent;
	public event MonoBaseCollision2D Collision2DStayEvent;
	public event MonoBaseCollision2D Collision2DExitEvent;
	public event MonoBaseTrigger TriggerEnterEvent;
	public event MonoBaseTrigger TriggerStayEvent;
	public event MonoBaseTrigger TriggerExitEvent;
	public event MonoBaseTrigger2D Trigger2DEnterEvent;
	public event MonoBaseTrigger2D Trigger2DStayEvent;
	public event MonoBaseTrigger2D Trigger2DExitEvent;

	void Awake()
	{
		if (this.AwakeEvent != null) {
			this.AwakeEvent();
		}
	}

	void Start()
	{
		if (this.StartEvent != null) {
			this.StartEvent();
		}
	}


	void Update()
	{
		if (this.UpdateEvent != null) {
			this.UpdateEvent();
		}
	}

	void FixedUpdate()
	{
		if (this.FixedUpdateEvent != null) {
			this.FixedUpdateEvent();
		}
	}

	void OnCollisionEnter(Collision c)
	{
		if (this.CollisionEnterEvent != null) {
			this.CollisionEnterEvent(c);
		}
	}

	void OnCollisionStay(Collision c)
	{
		if (this.CollisionStayEvent != null) {
			this.CollisionStayEvent(c);
		}
	}

	void OnCollisionExit(Collision c)
	{
		if (this.CollisionExitEvent != null) {
			this.CollisionExitEvent(c);
		}
	}

	void OnCollisionEnter2D(Collision2D c)
	{
		if (this.Collision2DEnterEvent != null) {
			this.Collision2DEnterEvent(c);
		}
	}

	void OnCollisionStay2D(Collision2D c)
	{
		if (this.Collision2DStayEvent != null) {
			this.Collision2DStayEvent(c);
		}
	}

	void OnCollisionExit2D (Collision2D c)
	{
		if (this.Collision2DExitEvent != null) {
			this.Collision2DExitEvent(c);
		}
	}

	void OnTriggerEnter(Collider c)
	{
		if (this.TriggerEnterEvent != null) {
			this.TriggerEnterEvent(c);
		}
	}

	void OnTriggerStay(Collider c)
	{
		if (this.TriggerStayEvent != null) {
			this.TriggerStayEvent(c);
		}
	}

	void OnTriggerExit(Collider c)
	{
		if(this.TriggerExitEvent != null)
		{
			this.TriggerExitEvent(c);
		}
	}     

	void OnTriggerEnter2D(Collider2D c)
	{
		if (this.Trigger2DEnterEvent != null) {
			this.Trigger2DEnterEvent(c);
		}
	}

	void OnTriggerStay2D(Collider2D c)
	{
		if (this.Trigger2DStayEvent != null) {
			this.Trigger2DStayEvent(c);
		}
	}

	void OnTriggerExit2D(Collider2D c)
	{
		if (this.Trigger2DExitEvent != null) {
			this.Trigger2DExitEvent(c);
		}
	}
}
