﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public delegate void AgentStateAction();
public class AgentStateEvents{
	
	private List<AgentStateEvent> _events = new List<AgentStateEvent> ();
	
	public AgentStateEvents()
	{
		foreach (AgentState state in (AgentState[])Enum.GetValues(typeof(AgentState))) {
			this._events.Add(new AgentStateEvent(state));
		}
	}
	
	public AgentStateEvent this[AgentState state]{
		get{
			return this._events.Find(x => x.State == state);
		}
	}
}

public class AgentStateEvent{
	private AgentState _state;
	private event AgentStateAction _events;
	
	public AgentStateEvent(AgentState state)
	{
		this._state = state; 
		this._events = null;
	}
	
	public AgentState State{
		get{
			return this._state;
		}
	}
	
	public AgentStateEvent Add(AgentStateAction action)
	{
		this._events += action;
		return this;
	}
	
	public AgentStateEvent Remove(AgentStateAction action)
	{
		this._events -= action;
		return this;
	}
	
	public void Run()
	{
		if (this._events != null) {
			this._events ();
		} 
	}
}