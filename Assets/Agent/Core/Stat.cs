﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void StatEvent();
public class Stat{

	private int _current;
	private int _max;
	private List<StatLevelEvent> _events = new List<StatLevelEvent> ();

	public Stat(int max)
	{
		this._current = max; 
		this._max = max;
	}

	public int Current{
		get{
			return this._current;
		}
	}

	public int Max{
		get{
			return this._max;
		}

		set{
			int k = this._max + value;
			if(k > 0)
			{
				this._current = (int)(this.Ratio*k);
				this._max = k; 
			}
		}
	}

	public float Ratio{
		get{
			return (float)this._current/(float)this._max;
		}
	}

	/**
	 * Method for changing stats current value. Return chng amnt. 
	 */
	public int Change(float value){
		int r = (int)Mathf.Clamp(this._current+value, 0, this._max);
		int dif = r - this._current;
		this._current = r;

		StatLevelEvent e = this._events.Find (x => x.Value == this._current);
		if (e != null) {
			e.Trigger();
		}

		return dif;
	}

	public StatLevelEvent this[int val]{
		get{
			StatLevelEvent e = this._events.Find(x => x.Value == val);
			if(e == null)
			{
				e = new StatLevelEvent();
				e.Value = val;
				this._events.Add(e);
			}
			return e;
		}
	}
}

public class StatLevelEvent{
	public int Value;
	public event StatEvent Event;
	public void Trigger()
	{
		if (this.Event != null) {
			this.Event();
		}
	}
}