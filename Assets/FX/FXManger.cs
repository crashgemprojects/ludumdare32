﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class FXManger : MonoBehaviour {

	void Awake()
	{
		GetComponent<ParticleSystem> ().Play (true);
	}

	// Update is called once per frame
	void Update () {
	
		if (!GetComponent<ParticleSystem> ().IsAlive ()) {
			Destroy(this.gameObject);
		}
	}
}
