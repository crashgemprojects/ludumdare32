﻿using UnityEngine;
using System;
using System.Collections;

public interface iSpawner: IEquatable<iSpawner>{
	int Id{get;}
	bool Contains(Vector2 pt);
}
