﻿using UnityEngine;
using System;
using System.Collections;

public class DruidHouse : GIA, iSpawner{

	public DruidHouse(AgentTemplate template, Vector3 position, Quaternion rotation):base(template, position, rotation)
	{
		this.mBase.StartEvent += () => {
			Vector2 pt = Round.Self.transform.position + Round.Self.gameObject.GetComponent<SpriteRenderer>().bounds.center;
			
			Ray2D r = new Ray2D(this.Transform.position, ((Vector2)this.Transform.position - pt).normalized);
			PC a = new PC(Round.Self.PlayerTemplate, r.GetPoint(1f), Quaternion.identity);
			Camera.main.GetComponent<FollowCamera2D>().Target = a;
		};
	}

	#region iSpawner implementation

	public bool Contains (Vector2 pt)
	{
		Vector3 size = this.gameObject.GetComponent<SpriteRenderer>().bounds.size;
		Bounds bounds = new Bounds (this.Transform.position, size);
		return bounds.Contains (pt);
	}

	public int Id {
		get {
			return this.gameObject.GetInstanceID();
		}
	}

	#endregion

	#region IEquatable implementation

	public bool Equals (iSpawner other)
	{
		return other.Id == this.Id;
	}

	#endregion
}
