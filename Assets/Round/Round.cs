using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer))]
public class Round : MonoBehaviour
{

	public static Round Self;
	public AgentTemplate PlayerTemplate;
	public AgentTemplate TreeTemplate;
	public AgentTemplate DruidHousePrefab;
	public AgentTemplate MoleHillPrefab;

	public ParticleSystem MagicPuff;

	public float HillSpawnMaxTimeInSeconds;
	public float HillSpawnMinTimeInSeconds;
	public int MaxMoleHills;
	public float SpawnAreaWidth;
	public float SpawnAreaHeight;
	public int MaxAgents;
	public List<AgentTemplate> RoundEnemies = new List<AgentTemplate> ();
	private DruidHouse _house;
	private List<iSpawner> _spawners = new List<iSpawner> ();
	private List<iSpawner> _placed = new List<iSpawner> ();
	private List<iTargetable> _targets = new List<iTargetable> ();

	void Awake ()
	{
		if (Self == null) {
			Self = this;

			if (this.DruidHousePrefab != null) {
				this._house = new DruidHouse (this.DruidHousePrefab, gameObject.GetComponent<SpriteRenderer> ().bounds.center, Quaternion.identity);
				this._placed.Add (this._house);
			} else {
				Debug.LogWarning ("No Druid House set for spawn.");
			}

			StartCoroutine (this._placeHill ());

		} else {
			Destroy (this.gameObject);
		}
	}

	void OnDrawGizmos ()
	{
		SpriteRenderer r = GetComponent<SpriteRenderer> ();

		Vector2 topLeft = new Vector2 (r.bounds.center.x - this.SpawnAreaWidth, r.bounds.center.y + this.SpawnAreaHeight) + (Vector2)this.transform.position;
		Vector2 topRight = new Vector2 (r.bounds.center.x + this.SpawnAreaWidth, r.bounds.center.y + this.SpawnAreaHeight) + (Vector2)this.transform.position;
		Vector2 bottomLeft = new Vector2 (r.bounds.center.x - this.SpawnAreaWidth, r.bounds.center.y - this.SpawnAreaHeight) + (Vector2)this.transform.position;
		Vector2 bottomRight = new Vector2 (r.bounds.center.x + this.SpawnAreaWidth, r.bounds.center.y - this.SpawnAreaHeight) + (Vector2)this.transform.position;

		Gizmos.DrawLine (topLeft, topRight);
		Gizmos.DrawLine (topRight, bottomRight);
		Gizmos.DrawLine (bottomRight, bottomLeft);
		Gizmos.DrawLine (bottomLeft, topLeft);

	}

	public int CurrentAgentCount {
		get {
			return this._targets.Count;
		}
	}

	public bool CanSpawnAgents {
		get {
			return this.CurrentAgentCount <= this.MaxAgents;
		}
	}

	private IEnumerator _placeHill ()
	{
		yield return new WaitForSeconds (Random.Range (this.HillSpawnMinTimeInSeconds, this.HillSpawnMaxTimeInSeconds));

		if (this._spawners.Count < this.MaxMoleHills) {
			Debug.Log ("Placing mole hill");
			this._placeMoleHill ();
		}


		StartCoroutine (this._placeHill ());

	}

	public DruidHouse House {
		get {
			return this._house;
		}
	}

	public void AddSpawner (iSpawner spawner)
	{
		if (spawner != null) {
			this._spawners.Add (spawner);
			this._placed.Add (spawner);
		}
	}

	public void RemoveSpawner (iSpawner spawner)
	{
		if (this._placed.Contains (spawner)) {
			this._placed.Remove (spawner);
			this._spawners.Remove (spawner);
		}
	}

	public void AddAgent (iTargetable target)
	{
		this._targets.Add (target);
	}

	public void RemoveAgent (iTargetable target)
	{
		this._targets.Remove (target);
		target = null;
	}

	public AgentTemplate CurrentEnemySpawnerTemplate {
		get {
			if (this.RoundEnemies.Count > 0) {
				if (this.RoundEnemies.Count > 1) {

					int k = Random.Range(0, this.RoundEnemies.Count);

					return this.RoundEnemies [k];
				} else {
					return this.RoundEnemies [0];
				}
			} else {
				Debug.LogError ("No round enemies set");
				return null;
			}
		}
	}

	private void _placeMoleHill ()
	{
		if (this.MoleHillPrefab != null) {
			MoleHill hill = new MoleHill (this.MoleHillPrefab, this._getPlaceablePoint (), Quaternion.identity);
		}
	}

	private Vector2 _getPlaceablePoint ()
	{
		SpriteRenderer r = GetComponent<SpriteRenderer> ();


		bool valid = false; 
		float xPos = 0;
		float yPos = 0;
		Vector2 pt = Vector2.zero;

		int maxPasses = 10;
		int passes = 0;

		while (!valid) {

			Vector2 topLeft = new Vector2 (r.bounds.center.x - this.SpawnAreaWidth, r.bounds.center.y + this.SpawnAreaHeight) + (Vector2)this.transform.position;
			Vector2 topRight = new Vector2 (r.bounds.center.x + this.SpawnAreaWidth, r.bounds.center.y + this.SpawnAreaHeight) + (Vector2)this.transform.position;
			Vector2 bottomLeft = new Vector2 (r.bounds.center.x - this.SpawnAreaWidth, r.bounds.center.y - this.SpawnAreaHeight) + (Vector2)this.transform.position;
			Vector2 bottomRight = new Vector2 (r.bounds.center.x + this.SpawnAreaWidth, r.bounds.center.y - this.SpawnAreaHeight) + (Vector2)this.transform.position;
			xPos = Random.Range ((r.bounds.center.x - this.SpawnAreaWidth) + this.transform.position.x, (r.bounds.center.x + this.SpawnAreaWidth) + this.transform.position.x);
			yPos = Random.Range ((r.bounds.center.y - this.SpawnAreaHeight) + this.transform.position.y, (r.bounds.center.y + this.SpawnAreaHeight) + this.transform.position.y);

			pt = new Vector2 (xPos, yPos) - (Vector2)r.bounds.center;
			bool empty = true;

			if (this._placed.Count > 0) {
				foreach (iSpawner spawner in this._placed) {
					if (spawner.Contains (pt)) {
						empty = false;
						break;
					}
				}
			} else {
				valid = true;
			}

			if (empty) {
				valid = true;
			}
			passes++;

			if (passes == maxPasses) {
				valid = true;
			}
		}

		return pt;
	}

}
