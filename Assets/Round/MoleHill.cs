﻿using UnityEngine;
using System.Collections;
using System;

public enum MoleHillState{
	Growing, 
	Active, 
	Dead
}

//Need to handle animations


public class MoleHill : GIA, iSpawner{
	
	private float _spawnTimeInSeconds = 4;
	private float _growingTime;
	private MoleHillState _state;
	private AgentStateEvents _events = new AgentStateEvents ();
	private bool _spawining = false;
	
	public MoleHill(AgentTemplate template, Vector3 position, Quaternion rotation):base(template, position, rotation)
	{
		Round.Self.AddSpawner (this);

		this.mBase.UpdateEvent += () => {
			if(this.grown && !this._spawining)
			{
				this._spawining = true;
				this.mBase.StartCoroutine(this._spawner());
			}
		};

		this.Events [AgentState.Die].Add (() => {
			Round.Self.RemoveSpawner(this);
		});
	}

	#region iSpawner implementation
	public bool Contains (Vector2 pt)
	{
		Vector3 size = this.gameObject.GetComponent<SpriteRenderer>().bounds.size;
		Bounds bounds = new Bounds (this.Transform.position, size);
		return bounds.Contains (pt);
	}

	public int Id {
		get {
			throw new NotImplementedException ();
		}
	}
	#endregion

	#region IEquatable implementation
	public bool Equals (iSpawner other)
	{
		throw new NotImplementedException ();
	}
	#endregion

	private IEnumerator _spawner()
	{
		yield return new WaitForSeconds (3f);

		if (Round.Self.CanSpawnAgents) {
			NPC npc = new NPC (Round.Self.CurrentEnemySpawnerTemplate, this.Transform.position, Quaternion.identity);
			npc.SetTarget (Round.Self.House);

			Physics2D.IgnoreCollision (gameObject.GetComponent<BoxCollider2D> (), npc.gameObject.GetComponent<BoxCollider2D> ());
			this.mBase.StartCoroutine (this._spawner ());
			this._spawining = false;
		}
	}

	private IEnumerator _growing()
	{
		this._state = MoleHillState.Growing;
		yield return null;
	}
}
