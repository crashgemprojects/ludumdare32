﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void ProjectileEffect(Creature c);

[RequireComponent(typeof(CircleCollider2D), typeof(Rigidbody2D))]
public class Projectile : MonoBase {

	public float Speed;
	public float Range;
	[Range(0,100)]
	public int Damage;
	protected Creature firer;
	protected ProjectileEffect secondaryEffect;
	private bool _fired;
	private Vector2 _firePoint;

	public Projectile(){
		this.UpdateEvent += () => {
			if(Vector2.Distance(this._firePoint, this.transform.position) >= this.Range)
			{
				Destroy(this.gameObject);
			}
		};

		this.Collision2DEnterEvent += this._impact;
	}

	public AgentSide Side{
		get{
			if(this.firer != null)
			{
				return this.firer.Side;
			}else{
				return AgentSide.None;
			}
		}
	}

	private void _impact(Collision2D c)
	{

		if (c.collider.GetComponent<MonoAgent> ()) {

		

			Agent agent = c.collider.GetComponent<MonoAgent> ().Agent;
			if (agent.Side != this.firer.Side) {
				if (agent.Type == AgentType.NPC || agent.Type == AgentType.PC) {
					Creature creature = (Creature)agent;
					creature.TakeDamage (this.Damage);
				}
			} else {
				Physics2D.IgnoreCollision (c.collider, this.GetComponent<Collider2D> ());
			}

			Destroy(this.gameObject);
		} else if (c.collider.GetComponent<Projectile> ()) {

			if(c.collider.GetComponent<Projectile>().Side == this.Side)
			{
				Physics2D.IgnoreCollision(c.collider, this.GetComponent<Collider2D>());
			}else{
				Destroy(this.gameObject);
			}
		}else{
			Destroy(this.gameObject);
		}
	}

	public void Fire(Vector3 direction, Creature firer) //Use direction here
	{
		this.firer = firer;
		this._firePoint = this.firer.Transform.position;
		float angle = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg ;

		this.transform.eulerAngles = new Vector3(0, 0, angle);

		Rigidbody2D r = GetComponent<Rigidbody2D> ();
		r.gravityScale = 0;
		r.fixedAngle = true;
		r.AddForce (direction * ( 10*this.Speed), ForceMode2D.Force);
	}

	public bool HasSecondary{
		get{
			return this.secondaryEffect == null;
		}
	}

	public void UseSecondary(Creature c)
	{
		if (this.secondaryEffect != null) {
			this.secondaryEffect(c);
		}
	}
}
