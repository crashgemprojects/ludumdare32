﻿using UnityEngine;
using System.Collections;

public interface iTargetable{
	Transform Transform{ get; }
	int TakeDamage(int damage);
	float TargetWeight{ get; }
	AgentStateEvents Events{ get;}
}
